cd "${0%/*}"
psql postgres -c "CREATE DATABASE football_bracket_test;"
psql football_bracket_test < ./src/database/football_bracket.sql
psql postgres -c "CREATE DATABASE football_bracket;"
psql football_bracket < ./src/database/football_bracket.sql