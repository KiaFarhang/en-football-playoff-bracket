import * as pgPromise from 'pg-promise';
import { IDatabase, IMain, ColumnSet, PreparedStatement } from 'pg-promise';

import IWhere from '../interfaces/IWhere';

import * as dotenv from 'dotenv';
dotenv.config();

// If we're in test mode, we set noLocking to true so we can override pg-promise methods in stubs
export const pgp: IMain = pgPromise({ noLocking: process.env.TEST ? true : false });

const connectionDetails = {
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT),
    database: process.env.TEST ? process.env.TEST_DB : process.env.DB,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD
};

export const db: IDatabase<any> = pgp(connectionDetails);

export const selectWhere = async (db: IDatabase<any>, table: string, { condition, target }: IWhere): Promise<object | object[]> | null => {
    try {

        // We need to wrap the target query in quotes if it's a string
        if (typeof target === 'string') {
            target = `'${target}'`;
        }

        // Query syntax is a bit different for null checks, hence the ternary

        const data = await db.any(`SELECT * FROM ${table} WHERE ${condition} ${target === null ? `IS NULL` : '= ' + target}`);

        if (data.length === 1) {
            return data[0];
        } else if (data.length === 0) {
            return null;
        }
        return data;
    }
    catch (e) {
        throw new Error(e);
    }
}

export const insertBracket: PreparedStatement = new PreparedStatement('insert-bracket', 'INSERT INTO brackets(name, email, bracket, divisionconference) VALUES($1,$2,$3,$4) RETURNING id');

export const selectBracketID: PreparedStatement = new PreparedStatement('select-bracket-id', 'SELECT ID FROM brackets WHERE email = $1 AND divisionconference = $2');

export const updateBracket: PreparedStatement = new PreparedStatement('update-bracket', 'UPDATE brackets SET bracket = $1 WHERE ID = $2');