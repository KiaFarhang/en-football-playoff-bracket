# Database Structure #

This project's database is implemented in PostgreSQL.

## Tables ##

### Brackets ###

Stores user brackets in JSON format. This table is meant soley for simple reads and writes - grab the JSON you need and do whatever manipulation/processing outside of SQL!

Uses the PostgreSQL pseudo_encrypt function to generate pseudo-random, unique IDs for each bracket. We keep all brackets - regardless of division and conference - in this table; the tradeoffs necessry to create a separate table for each permutation did not seem worth it. Therefore we also keep a `divisionconference` column to hold the string representation of which division and conference a given bracket corresponds to.


| PRIMARY id(bigint, not null)  | name(text) | email(text) | bracket(json) | divisionconference(text) | 
| ---    | ---    | ---   | ---   | ---  | 
| 6616518916819781  | 'Jim Smith'  | 'jsmith@email.com' | '{"name": "Tournament"...}' | 'div1_1a' |
