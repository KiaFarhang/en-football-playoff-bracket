--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: pseudo_encrypt(bigint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION pseudo_encrypt(value bigint) RETURNS bigint
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
DECLARE
l1 bigint;
l2 bigint;
r1 bigint;
r2 bigint;
i int:=0;
BEGIN
    l1:= (VALUE >> 32) & 4294967295::bigint;
    r1:= VALUE & 4294967295;
    WHILE i < 3 LOOP
        l2 := r1;
        r2 := l1 # ((((1520.0 * r1 + 5889) % 714025) / 714025.0) * 32*327)::int;
        l1 := l2;
        r1 := r2;
    i := i + 1;
    END LOOP;
RETURN ((r1::bigint << 32) + l1);
END;
$$;


--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: brackets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE brackets (
    id bigint DEFAULT pseudo_encrypt(nextval('user_id_seq'::regclass)) NOT NULL,
    name text,
    email text,
    bracket json,
    divisionconference text
);


--
-- Name: db_testing; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE db_testing (
    id integer NOT NULL,
    name text,
    year integer
);


--
-- Name: dbtesting_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE dbtesting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dbtesting_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE dbtesting_id_seq OWNED BY db_testing.id;


--
-- Name: db_testing id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY db_testing ALTER COLUMN id SET DEFAULT nextval('dbtesting_id_seq'::regclass);


--
-- Data for Name: brackets; Type: TABLE DATA; Schema: public; Owner: -
--

COPY brackets (id, name, email, bracket, divisionconference) FROM stdin;
4200648676738369061	\N	kia@google.com	{"foo": "bar"}	div1_1a
3337391585863193734	\N	\N	{"name":"Worlds Greatest Tournament","games":[{"location":"Foobar Zone","time":"7pm","nodes":[{"id":1,"team":{"name":"Team A"},"childID":5},{"id":2,"team":{"name":"Team B"},"childID":5}]},{"location":"Foobar Zone","time":"7pm","nodes":[{"id":3,"team":{"name":"Team C"},"childID":6},{"id":4,"team":{"name":"Team D"},"childID":6}]},{"location":"Foobar Zone","time":"7pm","nodes":[{"id":5,"team":null,"childID":7,"parentIDs":[1,2]},{"id":6,"team":null,"childID":7,"parentIDs":[3,4]}]}],"champion":{"id":7,"team":null,"parentIDs":[5,6]}}	div1_1a
\.


--
-- Data for Name: db_testing; Type: TABLE DATA; Schema: public; Owner: -
--

COPY db_testing (id, name, year) FROM stdin;
1	John	15
2	Jody	10
3	Jake	16
4	Jill	15
5	Jordan	\N
6	judy	\N
\.


--
-- Name: dbtesting_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('dbtesting_id_seq', 6, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('user_id_seq', 22, true);


--
-- Name: brackets brackets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY brackets
    ADD CONSTRAINT brackets_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

