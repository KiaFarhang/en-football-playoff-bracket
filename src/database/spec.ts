import 'mocha';
import * as chai from 'chai';
const assert = chai.assert;

import { db, selectWhere } from './index';

describe('Database', () => {
    describe('selectWhere', () => {
        describe('match found', () => {
            describe('one row match', () => {
                it('returns an object', async () => {
                    const result = await selectWhere(db, 'db_testing', { condition: 'name', target: 'John' });
                    assert.isObject(result);
                });
            });
            describe('multi-row match', () => {
                let results;
                before(async () => {
                    results = await selectWhere(db, 'db_testing', { condition: 'year', target: 15 });
                })
                it('returns an array', async () => {
                    assert.isArray(results);
                });
                it('each item in the array is an object', async () => {
                    results.forEach(item => assert.isObject(item));
                });
            });
        });
        describe('no match', () => {
            it('returns null', async () => {
                const results = await selectWhere(db, 'db_testing', { condition: 'year', target: 100 });
                assert.isNull(null);
            });
        });
        describe('table does not exist', () => {
            it('throws an error', async () => {
                let bad;
                try {
                    bad = await selectWhere(db, 'foobar', { condition: 'year', target: 15 });
                } catch (e) {
                    bad = e;
                }
                assert.typeOf(bad, 'Error');
            });
        });
        describe('condition is on a nonexistent column', () => {
            it('throws an error', async () => {
                let bad;
                try {
                    bad = await selectWhere(db, 'testing', { condition: 'foobar', target: 15 });
                } catch (e) {
                    bad = e;
                }

                assert.typeOf(bad, 'Error');
            });
        });
    });
});