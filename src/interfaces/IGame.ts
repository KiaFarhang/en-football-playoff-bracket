import INode from './INode';

export default interface IGame {
    location: string;
    time: string;
    nodes: [INode, INode];
}