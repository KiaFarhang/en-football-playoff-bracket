export default interface IWhere {
    condition: string;
    target: string | number | null;
}