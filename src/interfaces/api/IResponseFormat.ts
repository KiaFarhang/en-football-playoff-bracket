export default interface IResponseFormat {
    error: string | null;
    data: null | object | object[];
}