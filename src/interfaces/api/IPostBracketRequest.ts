import IBracket from '../IBracket';

export default interface IPostBracketRequest {
    bracket: IBracket,
    name?: string;
    email: string;
    conferenceDivision: string;
}