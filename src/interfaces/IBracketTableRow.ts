export default interface IBracketTableRow {
    id: string;
    name: string;
    email: string;
    bracket: object;
    divisionconference: string;
}