import IGame from '../interfaces/IGame';
import INode from '../interfaces/INode';

export default interface IBracket {
    name: string;
    games: IGame[];
    champion: INode;
    identifier?: string | number;
}