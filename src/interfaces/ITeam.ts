export default interface ITeam {
    name: string;
    logo?: string;
    colors?: [string, string];
}