import ITeam from './ITeam';

export default interface INode {
    id: number;
    team: ITeam;
    childID?: number;
    parentIDs?: [number, number];
}