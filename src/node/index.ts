import INode from '../interfaces/INode';
import ITeam from '../interfaces/ITeam';

import { isTeam } from '../team';

export const createNode = (id: number, team: ITeam | null, childID?: number, parentIDs?: [number, number]): INode => {
    let node = { id: id, team: team };
    if (childID) {
        node = Object.assign({}, node, { childID: childID });
    }
    if (parentIDs) {
        node = Object.assign({}, node, { parentIDs: parentIDs });
    }

    return Object.freeze(node);
}

export const getParentIDs = (node: INode): [number, number] | undefined => {
    if (node.parentIDs) return node.parentIDs;
    return undefined;
}

export const isNode = (object: object): object is INode => {
    if (Object.getOwnPropertyNames(object).length > 4 || Object.getOwnPropertyNames(object).length < 1) return false;
    const { id, team, parentIDs, childID } = <INode>object;
    if (!Number.isNaN(id) && typeof id === 'number') {
        if (!isTeam(team) && team !== null) return false;

        if (childID && typeof childID !== 'number') return false;

        if (parentIDs) {
            if (!Array.isArray(parentIDs)) return false;
            else {
                let flag = null;
                parentIDs.forEach((parentID) => {
                    if (Number.isNaN(parentID) || typeof parentID !== 'number') flag = false;
                });
                if (flag === false) return false;
            }
        }

        return true;
    }

    return false;

}