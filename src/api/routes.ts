import * as express from 'express';

import { db, selectWhere, insertBracket, selectBracketID, updateBracket } from '../database';

import IResponseFormat from '../interfaces/api/IResponseFormat';
import IPostBracketRequest from '../interfaces/api/IPostBracketRequest';
import IBracketTableRow from '../interfaces/IBracketTableRow';
import IBracket from '../interfaces/IBracket';
import IGame from '../interfaces/IGame';

import canonicalBrackets from './canonicalBrackets';

import { isBracket } from '../bracket';

const CUTOFF_DATE = new Date(Date.UTC(2017, 10, 17, 1, 0, 0, 0));

export const baseGetHandler = (req: express.Request, res: express.Response): void => {
    res.status(200);
    res.send();
}

export const canonicalGetHandler = (req: express.Request, res: express.Response): void => {
    const response: IResponseFormat = {
        data: canonicalBrackets,
        error: null
    };

    res.status(200);
    res.json(response);
}

export const getBracketHandler = async (req: express.Request, res: express.Response): Promise<void> => {
    try {
        const id = req.params['bracketid'];
        // If ID can't be converted to a number, it's probably shady and we should bail.

        if (Number.isNaN(parseInt(id))) {
            const response: IResponseFormat = {
                data: null,
                error: 'Please send a valid bracket ID'
            };
            res.status(400);
            res.json(response);
            return;
        }
        const data = await selectWhere(db, 'brackets', { condition: 'id', target: id });
        res.status(200);
        if (data === null) {
            const response: IResponseFormat = {
                data: null,
                error: 'No bracket matching that ID.'
            };
            res.json(response);
        } else if (isBracketTableRow(data)) {
            const { bracket, divisionconference } = data;
            const response: IResponseFormat = {
                data: {
                    bracket: bracket,
                    divisionConference: divisionconference
                },
                error: null
            };
            res.json(response);
        }

    } catch (e) {
        res.status(500);
        res.json(fetchError());
    }
}

export const postBracketHandler = async (req: express.Request, res: express.Response): Promise<void> => {

    // Do nothing if it's too late to update or create a bracket

    const currentDate = new Date();
    if (currentDate > CUTOFF_DATE) {
        const response: IResponseFormat = {
            error: `Sorry, it's too late to create or update a bracket!`,
            data: null
        };
        res.status(200);
        res.json(response);
    } else {
        // Validation - bail if request is off

        // const data: IPostBracketRequest = req.body;
        const { name, email, bracket, conferenceDivision } = req.body;
        if (!bracket || !email || !conferenceDivision) {
            send400Response();
        } else if (!isEmail(email)) {
            send400Response('Please use a valid email address');
        }
        else if (!isBracket(bracket)) {
            send400Response('Please send a valid bracket');
        }
        else {
            try {

                const bracketRow = await db.oneOrNone(selectBracketID, [email, conferenceDivision]);

                if (bracketRow !== null) {
                    // Bracket already exists

                    // Update bracket at that id

                    await db.none(updateBracket, [bracket, bracketRow.id]);

                    const response: IResponseFormat = {
                        error: null,
                        data: {
                            created: false,
                            id: parseInt(bracketRow.id)
                        }
                    };

                    res.status(200);
                    res.json(response);

                } else {
                    // Bracket does not exist

                    // insert and return new bracket ID

                    const newIDRow = await db.one(insertBracket, [!name ? null : name, email, bracket, conferenceDivision]);
                    const response: IResponseFormat = {
                        error: null,
                        data: {
                            created: true,
                            id: parseInt(newIDRow.id)
                        }
                    };
                    res.status(200);
                    res.json(response);
                }
            } catch (e) {
                res.status(500);
                res.send();
            }
        }

        function send400Response(errorMessage?: string) {
            res.status(400);
            res.json({ data: null, error: !errorMessage ? 'Please send a valid bracket request' : errorMessage });
        }
    }

    // Pulled from an answer in this SO thread:
    // https://codereview.stackexchange.com/questions/65190/email-validation-using-javascript
    // Note that we're just storing the emails, so this is a minimal test.
    // Will require more validation if we ever display them.
    function isEmail(string: string): boolean {
        return /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(string);
    }
}

const fetchError = (): IResponseFormat => {
    return {
        error: 'Error fetching your data, please try again.',
        data: null
    }
}

const isBracketTableRow = (object: object): object is IBracketTableRow => {
    return (<IBracketTableRow>object).id !== undefined;
}