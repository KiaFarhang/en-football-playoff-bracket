import 'mocha';
import * as chai from 'chai';
chai.use(require('chai-http'));
const assert = chai.assert;

import * as sinon from 'sinon';
import * as d from '../database';

import * as express from 'express';
import app from './index';

import validBracket from '../bracket/dummy-bracket';

describe('API', () => {
    describe('Security', () => {
        describe('Headers', () => {
            let response, headers;
            before(async () => {
                response = await chai.request(app).get('/');
                headers = response.headers;
            });
            it('has DNS-prefetch-control set to off', () => {
                const header = headers['x-dns-prefetch-control'];
                assert.strictEqual(header, 'off');
            });
            it('has x-frame-options set to DENY', () => {
                const header = headers['x-frame-options'];
                assert.strictEqual(header, 'DENY');
            });
            it('sets x-powered-by to PHP 7.0.23', () => {
                const header = headers['x-powered-by'];
                assert.strictEqual(header, 'PHP/7.0.23');
            });
            it('sets strict-transport-security to 180 days + include subdomains', () => {
                const header = headers['strict-transport-security'];
                assert.strictEqual(header, 'max-age=15552000; includeSubDomains');
            });
            it('sets x-download-options to noopen', () => {
                const header = headers['x-download-options'];
                assert.strictEqual(header, 'noopen');
            });
            it('sets x-content-type-options to nosniff', () => {
                const header = headers['x-content-type-options'];
                assert.strictEqual(header, 'nosniff');
            });
            it('sets x-xss-protection to 1; mode=block', () => {
                const header = headers['x-xss-protection'];
                assert.strictEqual(header, '1; mode=block');
            });
        });
    });
    describe('Endpoints', () => {
        describe('GET', () => {
            describe('/canonical', () => {
                let response, body, data, error;
                before(async () => {
                    response = await chai.request(app).get('/canonical');
                    body = response.body;
                    data = body.data;
                    error = body.error;
                });
                it('returns a 200 status code', () => {
                    assert.strictEqual(response.status, 200);
                });
                it('returns a null error', () => {
                    assert.isNull(error);
                });
                it('returns data', () => {
                    assert.isDefined(data);
                    assert.isNotNull(data);
                });
                describe('Data', () => {
                    it('is an array', () => {
                        assert.isArray(data);
                    });
                    it('each item in the array is an object', () => {
                        data.forEach((item) => {
                            assert.isObject(item);
                        });
                    });
                    it('each item is a bracket', () => {
                        data.forEach((bracket) => {
                            assert.property(bracket, 'name');
                            assert.isString(bracket.name);
                            assert.property(bracket, 'games');
                            assert.isArray(bracket.games);
                            assert.property(bracket, 'champion');
                            assert.isObject(bracket.champion);
                        });
                    });
                });
            });
            describe('/bracket/:bracketID', () => {
                describe('error processing response', () => {
                    let response, body, data, error, stub;
                    before(async () => {
                        stub = sinon.stub(d, 'selectWhere');
                        stub.throws();
                        response = await chai.request(app).get('/bracket/11111');
                        body = response.body;
                        data = body.data;
                        error = body.error;
                    });
                    after(() => {
                        stub.restore();
                    });
                    it('returns a 500 status', () => {
                        assert.strictEqual(response.status, 500);
                    });
                    it('returns null data', () => {
                        assert.isNull(data);
                    });
                    it('returns a string error', () => {
                        assert.isString(error);
                    });
                });
                describe('successfully processes request', () => {
                    describe('malformed request', () => {
                        let response, body, data, error;
                        before(async () => {
                            response = await chai.request(app).get('/bracket/foobar');
                            body = response.body;
                            data = body.data;
                            error = body.error;
                        });
                        it('returns a 400 status code', () => {
                            assert.strictEqual(response.status, 400);
                        });
                        it('returns null data', () => {
                            assert.isNull(data);
                        });
                        it('returns a string error', () => {
                            assert.isString(error);
                        });
                    });
                    let response, body, data, error;
                    before(async () => {
                        response = await chai.request(app).get('/bracket/555');
                        body = response.body;
                        data = body.data;
                        error = body.error;

                    });
                    it('returns a 200 status code', () => {
                        assert.strictEqual(response.status, 200);
                    });
                    describe('bracket does not exist', () => {
                        let response, body, data, error;
                        before(async () => {
                            response = await chai.request(app).get('/bracket/555');
                            body = response.body;
                            data = body.data;
                            error = body.error;
                        });
                        it('returns null data', () => {
                            assert.isNull(data);
                        });
                        it('returns a string error', () => {
                            assert.isString(error);
                        });
                    });
                    describe('bracket exists', () => {
                        let response, body, data, error;
                        before(async () => {
                            response = await chai.request(app).get('/bracket/3337391585863193734');
                            body = response.body;
                            data = body.data;
                            error = body.error;
                        });
                        it('returns data as an object', () => {
                            assert.isNotNull(data);
                            assert.isObject(data);
                        });
                        it('the data has a bracket property, which is a bracket', () => {
                            const bracket = data.bracket;
                            assert.isObject(bracket);
                            assert.property(bracket, 'name');
                            assert.property(bracket, 'games');
                            assert.property(bracket, 'champion');
                            bracket.games.forEach((game) => {
                                assert.property(game, 'location');
                                assert.property(game, 'time');
                                assert.property(game, 'nodes');
                            });
                        });
                        it('returns a null error', () => {
                            assert.isNull(error);
                        });
                    });
                });
            });
        });
        describe('POST', () => {
            describe('/bracket', () => {
                const endpoint = '/bracket';
                describe('Malformed requests', () => {
                    let responses = [];
                    let bodies = [];
                    let data = [];
                    let errors = [];


                    before(async () => {

                        //Missing parameters
                        responses[0] = await chai.request(app)
                            .post(endpoint)
                            .send({ bracket: validBracket, conferenceDivision: 'Gimme ur SSN' })
                        bodies[0] = responses[0].body;
                        data[0] = bodies[0].data;
                        errors[0] = bodies[0].error;

                        //Invalid bracket
                        responses[1] = await chai.request(app)
                            .post(endpoint)
                            .send({ bracket: { foo: 'DELETE FROM brackets;' }, email: 'foo@bar.com', conferenceDivision: 'Gimme ur SSN' })
                        bodies[1] = responses[1].body;
                        data[1] = bodies[1].data;
                        errors[1] = bodies[1].error;

                        //Invalid email address
                        responses[2] = await chai.request(app)
                            .post(endpoint)
                            .send({ bracket: validBracket, email: 'qasd@', conferenceDivision: 'Gimme ur SSN' })
                        bodies[2] = responses[2].body;
                        data[2] = bodies[2].data;
                        errors[2] = bodies[2].error;

                    });
                    it('Sends a 400 status', () => {
                        responses.forEach((response) => {
                            assert.strictEqual(response.status, 400);
                        });
                    });
                    it('sends a string error message', () => {
                        errors.forEach((error) => {
                            assert.isString(error);
                        });
                    });
                });
                describe('Properly formatted requests', () => {
                    describe(`Email doesn't yet have a bracket for the given conference + division`, () => {
                        let response, body, data, error;

                        before(async () => {
                            response = await chai.request(app)
                                .post(endpoint)
                                .send({ bracket: validBracket, email: 'qwijibo@fizzbomb.zone', conferenceDivision: 'div1_1a' })
                            body = response.body;
                            data = body.data;
                            error = body.error;

                        });

                        after(async () => {
                            await d.db.none({
                                text: 'DELETE FROM brackets WHERE email = $1',
                                values: ['qwijibo@fizzbomb.zone']
                            });
                        })
                        it('returns a 200 status code', () => {
                            assert.strictEqual(response.status, 200);
                        });
                        it('returns a null error', () => {
                            assert.isNull(error);
                        });
                        it(`returns a data object`, () => {
                            assert.isObject(data);
                        });
                        it(`the data has an 'id' property, which is a number`, () => {
                            assert.property(data, 'id');
                            assert.isNumber(data.id);
                        });
                        it(`the data has a 'created' property, which is set to true`, () => {
                            assert.property(data, 'created');
                            assert.isTrue(data.created);
                        });
                        it('adds the bracket, email and conference + division to the brackets table in the database', async () => {
                            const newDBRow = await d.db.one({
                                text: 'SELECT * FROM brackets WHERE email=$1',
                                values: ['qwijibo@fizzbomb.zone']
                            });

                            assert.strictEqual(newDBRow.email, 'qwijibo@fizzbomb.zone');
                            assert.strictEqual(newDBRow.divisionconference, 'div1_1a');
                            assert.deepEqual(newDBRow.bracket.games, validBracket.games);
                            assert.deepEqual(newDBRow.bracket.champion, validBracket.champion);
                        });
                    });
                    describe(`Email already has a bracket for the given conference + division`, () => {
                        let response, body, data, error;

                        before(async () => {
                            response = await chai.request(app)
                                .post(endpoint)
                                .send({ bracket: validBracket, email: 'kia@google.com', conferenceDivision: 'div1_1a' })
                            body = response.body;
                            data = body.data;
                            error = body.error;
                        });

                        after(async () => {
                            await d.db.none(d.updateBracket, ['{"foo": "bar"}', '4200648676738369061']);
                        });
                        it('returns a 200 status', () => {
                            assert.strictEqual(response.status, 200);
                        });
                        it('returns a null error', () => {
                            assert.isNull(error);
                        });
                        it('returns a data object', () => {
                            assert.isObject(data);
                        });
                        it(`the data has a 'created' property, which is set to false`, () => {
                            assert.property(data, 'created');
                            assert.isFalse(data.created);
                        });
                        it(`the data has an 'id' property, which is a number`, () => {
                            assert.property(data, 'id');
                            assert.isNumber(data.id);
                        });
                        it('updates the bracket with the new bracket provided', async () => {
                            const dbResult = await d.db.one({
                                text: `SELECT bracket FROM brackets WHERE id = $1`,
                                values: ['4200648676738369061']
                            });

                            const bracket = dbResult.bracket;

                            assert.deepEqual(bracket.games, validBracket.games);
                            assert.deepEqual(bracket.champion, validBracket.champion);
                        });
                    });
                });
            });
        });
    });
});

