import * as express from 'express';
import * as helmet from 'helmet';
import * as path from 'path';
import * as morgan from 'morgan';
import * as fs from 'fs';
import * as rfs from 'rotating-file-stream';
import * as bodyParser from 'body-parser';

import * as routes from './routes';

const app = express();

//Set up helmet to protect the server
app.use(helmet({
    frameguard: { action: 'deny' },
    hidePoweredBy: { setTo: 'PHP/7.0.23' }
}));

const apiRouter = express.Router();

app.use('/', apiRouter);

//Create a log directory at the project root if it doesn't exist

const logDir = path.join(__dirname, '/../../', 'log');
fs.existsSync(logDir) || fs.mkdirSync(logDir);

//Create a daily rotating log file stream

const logStream = rfs('access.log', {
    interval: '1d',
    path: logDir
});

//Set up some basic logging

apiRouter.use(morgan('combined', { stream: logStream }));

// Use body-parser to handle JSON POST requests

apiRouter.use(bodyParser.json());

// Set up our routes

apiRouter.get('/', routes.baseGetHandler);
apiRouter.get('/canonical', routes.canonicalGetHandler);
apiRouter.get('/bracket/:bracketid', routes.getBracketHandler);
apiRouter.post('/bracket', routes.postBracketHandler);

export default app;
