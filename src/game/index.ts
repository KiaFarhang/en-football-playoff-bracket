import IGame from '../interfaces/IGame';
import INode from '../interfaces/INode';

import { isNode } from '../node';

export const createGame = (location: string, time: string, nodes: [INode, INode]): IGame => {
    return Object.freeze(Object.assign({}, { location: location }, { time: time }, { nodes: nodes }));
}

export const getNodesInGame = (game: IGame): [INode, INode] => {
    return game.nodes;
}

export const isGame = (object: object): object is IGame => {
    if (Object.getOwnPropertyNames(object).length > 3 || Object.getOwnPropertyNames(object).length < 1) return false;
    const { location, time, nodes } = <IGame>object;
    if (!location || !time || !nodes) return false;

    if (typeof location !== 'string') return false;

    if (typeof time !== 'string') return false;

    if (!Array.isArray(nodes)) return false;
    else {
        let flag = true;
        nodes.forEach((node) => {
            if (!isNode(node)) flag = false;
        });
        if (!flag) return false;
    }

    return true;

}