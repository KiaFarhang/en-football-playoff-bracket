import { isEqual, cloneDeep } from 'lodash';

import IBracket from '../interfaces/IBracket';
import IGame from '../interfaces/IGame';
import INode from '../interfaces/INode';
import ITeam from '../interfaces/ITeam';

import { createNode, getParentIDs, isNode } from '../node';
import { getNodesInGame, isGame } from '../game';

import * as utils from '../util';

export const createBracket = (name: string, games: IGame[], champion: INode): IBracket => {
    let nodes: INode[] = games.map((game => getNodesInGame(game))).reduce((a, b) => a.concat(b), []);
    nodes.push(champion);
    if (!utils.everyObjectHasUniquePropertyValue(nodes, 'id')) throw new Error('Every node in a bracket must have a unique ID');

    return Object.freeze(Object.assign({}, { name: name, games: games, champion: champion }));
}

export const getNodeAt = (bracket: IBracket, id: number): INode | undefined => {
    const findFunction = (node: INode): boolean => {
        return node.id === id;
    }

    return getAllNodesInBracket(bracket).find(findFunction);
}

export const getAllAccessibleAncestorIDs = (bracket: IBracket, id: number): number[] | undefined => {
    // Throw an error if the node doesn't exist

    if (nodeAtIDDoesNotExist(bracket, id)) throwNonExistentIDError();

    // Return undefined if the node exists but has no ancestors

    if (!getNodeAt(bracket, id).parentIDs) return undefined;

    // Move through the node's ancestors using the recursion utility function:

    // On any given node, we should add it if:
    // Its id doesn't match the ID we started at (so we only get ancestors)
    // The node's child has no team ()

    const collectCondition = (nodeID: number) => {
        //We want to collect direct parents of the node no matter what
        if (getNodeAt(bracket, id).parentIDs.indexOf(nodeID) > -1) return true;

        return nodeID !== id && getNodeAt(bracket, getNodeAt(bracket, nodeID).childID).team === null;
    }

    // Call the function again on any of the node's parents that have a null team

    const moveUpCondition = (nodeID: number) => {
        // We want to call the recursive function on direct parents of the node no matter what
        if (getNodeAt(bracket, id).parentIDs.indexOf(nodeID) > -1) return true;

        return nodeID !== id && getNodeAt(bracket, getNodeAt(bracket, nodeID).childID).team === null;
    }

    // Return the array of collected ancestor IDs

    return getIDsMatchingConditions(bracket, id, collectCondition, moveUpCondition);
}

export const getAllDescendentIDs = (bracket: IBracket, id: number): number[] | undefined => {
    // Throw an error if the node doesn't exist

    if (nodeAtIDDoesNotExist(bracket, id)) throwNonExistentIDError();

    // If node doesn't have a child, return undefined

    if (!getNodeAt(bracket, id).childID) return undefined;

    // Create an array to hold all descendent IDs

    let descendents = [];

    let currentNode = getNodeAt(bracket, id);

    // Keep adding descendents to the array until you reach a node with no child

    while (currentNode.childID) {
        descendents.push(currentNode.childID);
        currentNode = getNodeAt(bracket, currentNode.childID);
    }

    // Return the array

    return descendents;

}

//For a team update to be legal, the team we're trying to update a node with
//Must exist in that node's accessible ancestor chain

export const isTeamUpdateLegal = (bracket: IBracket, id: number, team: ITeam): boolean => {
    const accessibleAncestorIDs: number[] = getAllAccessibleAncestorIDs(bracket, id);

    if (!accessibleAncestorIDs) return false;

    const accessibleAncestorNodes: INode[] = accessibleAncestorIDs.map(id => getNodeAt(bracket, id));

    const doesTeamMatch = (node: INode): boolean => {
        return isEqual(node.team, team);
    }

    return accessibleAncestorNodes.find(doesTeamMatch) !== undefined;
}

export const updateTeamAtNode = (bracket: IBracket, id: number, team: ITeam | null): IBracket => {
    if (nodeAtIDDoesNotExist(bracket, id)) throwNonExistentIDError();

    let newBracket = cloneDeep(bracket);

    let nodes = getAllNodesInBracket(newBracket);
    nodes[id - 1].team = team;

    return Object.freeze(newBracket);
}

export const updateTeamAbove = (bracket: IBracket, id: number, team: ITeam): IBracket => {
    if (nodeAtIDDoesNotExist(bracket, id)) throwNonExistentIDError();

    // No ancestors to update if this is a root node, so just return the bracket

    if (!getNodeAt(bracket, id).parentIDs) return bracket;

    // Get all accessible ancestors of the given node

    const accessibleAncestors = getAllAccessibleAncestorIDs(bracket, id).map(id => getNodeAt(bracket, id));

    // Filter out any of the ancestors with a team

    const teamlessAncestors = accessibleAncestors.filter(node => node.team === null);

    if (teamlessAncestors.length === 0) return bracket;

    // Filter again, removing any nodes that don't have the team in their acccessible ancestor chain

    const ancestorsToUpdate = teamlessAncestors.filter(node => isTeamUpdateLegal(bracket, node.id, team));

    if (ancestorsToUpdate.length === 0) return bracket;
    let newBracket = cloneDeep(bracket);

    // For each node in that double filtered array, update its team to the new team

    ancestorsToUpdate.forEach((ancestorNode) => {
        newBracket = updateTeamAtNode(newBracket, ancestorNode.id, team);
    });

    // Return the updated bracket

    return newBracket;
}

export const nullTeamBelow = (bracket: IBracket, id: number, team: ITeam): IBracket => {
    if (nodeAtIDDoesNotExist(bracket, id)) throwNonExistentIDError();

    // Get all descendents of the node at given ID

    const descendents: INode[] = getAllDescendentIDs(bracket, id).map(id => getNodeAt(bracket, id));

    // For each child, if the child has a team matching the team passed, set that team to null

    let newBracket = cloneDeep(bracket);
    descendents.forEach((descendent: INode) => {
        if (isEqual(descendent.team, team)) newBracket = updateTeamAtNode(bracket, descendent.id, null);
    });

    // return a new bracket with the changes

    return newBracket;

}

export const isBracket = (object: Object): object is IBracket => {
    if (Object.getOwnPropertyNames(object).length > 4 || Object.getOwnPropertyNames(object).length < 1) return false;
    const { name, games, champion } = <IBracket>object;
    if (!name || !games || !champion) return false;

    if (typeof name !== 'string') return false;

    if (!isNode(champion)) return false;

    if (!Array.isArray(games)) return false;
    else {
        let flag = true;
        games.forEach((game) => {
            if (!isGame(game)) flag = false;
        });
        if (!flag) return false;
    }
    return true;
}

// This function moves recursively through a node and its ancestors to return an array of node IDs.
// The function starts on the given node, checking it against the addCondition function. If it returns true, the starting node is added to the array.
// The function then checks if the node has parents. If not, it's done and returns the array.
// If parents do exist, the function moves on to call itself on both parents.
// The optional recursionCondition is used to control whether you want to call the function on a parent.
// If passed, the function will only be called on parents that pass the recursionCondtion function test.

function getIDsMatchingConditions(bracket: IBracket, startingNodeID: number, addCondition: Function, recursionCondition?: Function): number[] {
    let matchingIDs: number[] = [];

    const recursion = (id: number): void => {
        if (addCondition(id)) matchingIDs.push(id);

        const parentIDs = getParentIDs(getNodeAt(bracket, id));

        if (parentIDs && recursionCondition) {
            parentIDs.forEach((parentID) => {
                if (recursionCondition(parentID)) recursion(parentID);
            });
        } else if (parentIDs) parentIDs.forEach((parentID => recursion(parentID)));
    }

    recursion(startingNodeID);

    return matchingIDs;
}

function nodeAtIDDoesNotExist(bracket: IBracket, id: number): boolean {
    return getNodeAt(bracket, id) === undefined;
}

function throwNonExistentIDError() {
    throw new Error('Bracket does not contain a node matching the given ID');
}

function getAllNodesInBracket(bracket: IBracket): INode[] {
    const games = bracket.games;
    let nodes: INode[] = bracket.games.map((game => getNodesInGame(game))).reduce((a, b) => a.concat(b), []);
    nodes.push(bracket.champion);

    return nodes;
}