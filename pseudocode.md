## API ##

### POST ###

#### /17footballbrackets ####

Endpoint that allows for the saving of brackets in the database. BE SURE TO TURN THIS OFF/RETURN AN ERROR AFTER THE CUTOFF. Expects a payload like:

```typescript
    bracket: Bracket;
    name?: string;
    email: string;
    conferenceDivision: string;
```

The endpoint should:

- Validate that the bracket passed is a valid bracket (duck type it)
- Make sure there's no creepy SQL in any of the string fields
- Do some email validation - it should be valid because it's coming from the EN cookie, but do it anyway.
- Select ID from the brackets `table`. If no match, insert everything into the `brackets` table in the database, returning the ID it generates.
- Remember to escape all apostrophes in the JSON! `"World's Greatest Bracket"` won't insert properly.
- If there is a match, user has already created a bracket for that conference and division. Set the response's `created` property to `false`.
- Send a 200 code back to the user with the following payload:

```typescript
    url: string;
    created: boolean;
```

- The URL will look something like `/:id`.
- The `created` property tells the page whether a new bracket was created, or whether this is a link to the bracket the user already set up (they can go there and change it if it's not too late).
- If anything fails along the way, send a 400 error (if they screwed up) or a 500 (if something on our end did).

### GET ###

#### /17footballbrackets/bracket/:bracketID ####

Endpoint to fetch a user's bracket.

Expects the following payload:

```typescript
    id: number;
```

The endpoint should: 

- Validate the ID is legitimate.
- If it is, select the bracket matching the ID in the database.
- Send a 200 code back to the user with the following payload:

```typescript
    bracket: Bracket;
    divConf: string;
```

- If there's no bracket matching that ID, send a 404.
- If anything fails along the way, send a 400 error (if they screwed up) or a 500 (if something on our end did).

#### /17footballbrackets/canonical ####

Endpoint to fetch up-to-date canonical brackets. Returns a payload like so:

```typescript
    data: {
        div1_a: Bracket,
        div2_a: Bracket
    }
```
Etc.

## Page ##

### /bracketwhatever ###

Main landing page. Upon load, the page should:

- Check paywall status (may need corporate script to do this)
- Draw the starting bracket for whatever conference + division is the default start. (All starting brackets will live in the Redux store described below, but at least one should be stored in the page).
- Grab the user's EN account email from cookie and store it in the Redux store, so it's easier to get later.
- Ping the canonical bracket API endpoint to fetch all canonical brackets, then update the Redux store with the data.

The current bracket data should live in a Redux store. Any change to the bracket on the page should trigger an action that updates the bracket. The React application should subscribe to the store and display the bracket accordingly.

Redux store:

```typescript
    brackets: {
        div1_a{
            canonical: Bracket;
            user: Bracket;
        },
        div1_b{
            canonical: Bracket;
            user: bracket;
        }
    }
    email: string;
```

Each user bracket should start as a copy of the canonical bracket; users will then update the `user` bracket for a given divison in the store and never touch the canonical one.

- The application should have a select box with options for each of the conferences and divisons. The values of those options will correspond to properties in the Redux store, so users can switch between brackets without losing data and take their time filling brackets out.

- When ready to send, user will fill out their name (optional) and hit the send button. They'll need to confirm their bracket is done - though we'll let them know they can still change their bracket up to the first game.

- We make a POST request to the server with bracket information. If anything goes wrong, display an error message.

- If everything works, update the URL with the new ID. Throw up a modal that says "Hey, we have your bracket. You should bookmark the current URL to access it later. If you want, you can switch to other brackets and send those too."

- Updating and sending a different bracket will do the same thing. If sending a bracket you've already sent, you'd get a modal saying "Thanks, we already have a bracket from you so we just updated that one."

### /bracketwhatever/:bracketID ###

Page to update a bracket before the cutoff or see how it compares after game starts.

- React Router should check the date any time it moves to a `/:bracketID` route. If before the cutoff, do nothing. If before the cutoff, do nothing - just stick with the current bracket creation/update stuff. If after the cutoff, move to bracket diffing as described below.

- On page load, application should:
- Make a network request to grab the canonical brackets.
- Grab the ID from the URL and hit the ID-specific endpoint to update the Redux store with the bracket. Use the `divConf` property of the JSON payload to choose which canonical bracket to diff against.
- If date is before cutoff, display an "Update" button that will trigger the same bracket update network request as the main page.