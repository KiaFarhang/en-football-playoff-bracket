# Express-News Bracket Creator #

## Getting Started ##

### Building the Database ###

This application uses a PostgreSQL database to store bracket information. Running the application test suite also requires you to set up a separate test database. This repo contains a shell script to populate the databases. Here's how to get everything working:

1. If your shell user doesn't yet have a PostgreSQL role, get to the `psql` command prompt and create one for it:

`su - postgres` 
`psql`
(or `sudo -u postgres psql postgres`)
`CREATE ROLE yourname WITH CREATEDB LOGIN ENCRYPTED PASSWORD 'goodpassword';`
`\q`

2. Navigate to this project's root directory and run the `create_db.sh` script:

`./create_db.sh`

You should see the results of a SQL database creation script. (You can ignore the warnings about privileges for "public").

*Note: if you're running an earlier version of the application, you should run `./drop_db.sh` first to drop the old versions of the database and ensure you're working off the most recent ones.*

3. Create a `.env` file in the project root formatted like so:

```
PORT=8000 
DB_HOST=localhost
DB_PORT=5432
DB=real_estate
TEST_DB=real_estate_test
DB_USER=your_user
DB_PASSWORD=your_pass
```

Where `DB_USER` and `DB_PASSWORD` match the role you created.

4. Run `npm run test` to ensure the test suite - including database access - works properly.

## Data Structures ##

There are four main data structures. We'll start from the smallest and work our way up:

### Team ###

Represents a team with the following properties:

```typescript
    name: string;
    logo?: string;
    colors?: [string, string];
```

Logo is meant to store an image path; colors is meant to store a tuple consisting of two hex codes.

#### Functions ####

##### createTeam(name: string, logo?: string, colors?: [string, string]): Team #####

Factory function used to create a team.

### Node ###

Represents a seat in a tournament, with the following properties:

```typescript
    id: number;
    team: Team | null;
    childID?: number;
    parentIDs?: [number, number];
```
Each node/seat has a numerical ID. If no team is currently occupying the node, `team` will be null; otherwise it will contain a reference to a team. Note that root nodes will always have a team.

Nodes can share a team: If I pick Team A to win the whole tournament, there will be several nodes within the bracket whose `team` will be Team A.

Every node except the winning seat has a child node and keeps a reference to its ID number. Every node except root nodes has two parents - the two teams that will play to reach the node - and keeps references to their ID numbers.

#### Functions ####

##### createNode(id: number, team: ITeam | null, childID?: number, parentIDs?: [number, number]): Node #####

Factory function to create a node.

##### getParentIDs(node: Node): [number, number] | undefined #####

Returns an array containing both of the node's parent IDs. Returns `undefined` if the node has no parents (and is thus a root node).

### Game ###

Represents a matchup between two nodes, with the following properties:

```typescript
    location: string;
    time: string;
    nodes: [Node, Node];
```

`location` represents where the game will be played. `time` is the time of the game (strings are fine for our purposes).

Games exist as a semi-transparent layer over nodes; we need them to display a bracket properly, but most of the work happens on the bracket and node level.

#### Functions ####

##### createGame(location: string, time: string, nodes: [Node, Node]): Game #####

Factory function for creating games.

##### getGamesInNode(game: Game): [Node, Node]) #####

Returns the two nodes in a given game.

### Bracket ###

Represents a bracket, with the following properties:

```typescript
    name: string;
    games: Game[];
    champion: Node;
```

A bracket is a collection of games with one winner node at the top level. To keep lower-level structures as dumb as possible, most of the pathfinding and updating code exists at the bracket level.

When working with brackets, it helps to tip them sideways: the first round is at the top, and each successive round below it.

#### Functions ####

##### createBracket(name: string, games: Game[], champion: Node): Bracket #####

Factory function to create a bracket.

##### getNodeAt(bracket: Bracket, id: number):Node | undefined ######

Returns the node at a given ID in the bracket. Returns `undefined` if there is no node at that ID.

##### getAllAccessibleAncestorIDs(bracket: Bracket, id: number): number[] | undefined #####

Returns a list of IDs corresponding to nodes that currently flow to the node at the ID passed. Used to find out whether a move is legal (i.e. whether the user can update a node with a given team). For example, in this bracket:

```
Team A (ID 1)
        Team A (ID 5)
Team B (ID 2)
                Teamless Node (ID 7)
Team C (ID 3)
        Team C (ID 6)
Team D (ID 4)
```

The teamless node can only have its team set to Team A or Team C. This function would return the IDs for the two second-round nodes with those teams.

If the bracket changes like so:

```
Team A (ID 1)
        Team A (ID 5)
Team B (ID 2)
                Teamless Node (ID 7)
Team C (ID 3)
        Teamless Node (ID 6)
Team D (ID 4)
```

There are now _three_ nodes that could change the final teamless node: the second-round Team A node, the first-round Team C node and the first-round Team D node. This function would return the IDs of those three nodes.

Note that in this case, the function would _not_ return the ID of the first-level Team A or Team B nodes. They're essentially blocked from the final teamless node, because there's a node with its team set in between them.

Returns `undefined` if the node in question is a root node and therefore has no ancestors. Every non-root node _will_ have at least two accessible ancestor IDs.

##### getAllDescendentIDs(bracket: Bracket, id: number): number[] | undefined #####

Collects every descendent of the node at the given ID. Returns `undefined` if the node in question is the final node in the bracket and thus has no children.

To take this example again: 

```
Team A (ID 1)
        Team A (ID 5)
Team B (ID 2)
                Teamless Node (ID 7)
Team C (ID 3)
        Teamless Node (ID 6)
Team D (ID 4)
```

Calling the function on ID 1 would return an array of `[5, 7]`. Calling it with 5 or 6 would return `[7]`.

##### isTeamUpdateLegal(bracket: Bracket, id: number, team: Team): boolean #####

Computes whether setting the node at the given ID to the given team is possible based on the structure of the current bracket.

Consider the following:

```
Team A (ID 1)
        Team A (ID 5)
Team B (ID 2)
                Teamless Node (ID 7)
Team C (ID 3)
        Teamless Node (ID 6)
Team D (ID 4)
```

I can't set the node at ID 7 to Team B, because Team B lost to Team A. Calling this function would return `false`. If I passed Team D, however, I'd get `true` - because I haven't picked whether D will beat C, so I could still move either of them into the champion spot.

Note that, in the second case lined out above, putting D in 7 would necessitate putting D in 6 as well. `updateTeamBelow`, another bracket-related function, does just that.

##### updateTeamAtNode(bracket: Bracket, id: number, team: Team | null): Bracket ##### 

Returns a new bracket where the node at the given ID has the given team (or `null`, if you're trying to reset a node). Note that legality is not checked at all; use the `isTeamUpdateLegal` function before this one for that.

If I pass the function this bracket:

```
Team A (ID 1)
        Team A (ID 5)
Team B (ID 2)
                Teamless Node (ID 7)
Team C (ID 3)
        Teamless Node (ID 6)
Team D (ID 4)
```

the ID 6 and the team D, the bracket returned will look like so:

```
Team A (ID 1)
        Team A (ID 5)
Team B (ID 2)
                Teamless Node (ID 7)
Team C (ID 3)
        Team D (ID 6)
Team D (ID 4)
```

##### updateTeamAbove(bracket: Bracket, id: number, team: Team): Bracket #####

Updates ancestors of a node with a given team as long as that move is also legal. Returns a new bracket with those updates applied. Does _not_ update the starting node, only its ancestors. Consider the following:

```
Team A (ID 1)
        Team A (ID 5)
Team B (ID 2)
                Teamless Node (ID 7)
Team C (ID 3)
        Teamless Node (ID 6)
Team D (ID 4)
```

Say I think Team D is going to win the whole thing, and I want to put that in node 7. I shouldn't have to drag it into 6 first, and I don't have to. 

First I assign Team D to node 7 using `updateTeamAtNode`:

```
Team A (ID 1)
        Team A (ID 5)
Team B (ID 2)
                Team D (ID 7)
Team C (ID 3)
        Teamless Node (ID 6)
Team D (ID 4)
```

Then I call this function - `updateTeamAbove` on node 7 with Team D and get this final bracket:

```
Team A (ID 1)
        Team A (ID 5)
Team B (ID 2)
                Team D (ID 7)
Team C (ID 3)
        Team D (ID 6)
Team D (ID 4)
```

Team D _had to_ have won the game with Team C. This function applies the update to node 6. In this case it's not super helpful because the bracket is small, but with larger brackets this function is easier than manually determining which upper nodes to update.

As mentioned above, the function would _not_ touch node 7 - use `updateTeamAtNode` for that.

##### nullTeamBelow(bracket: Bracket, id: number, team: Team): Bracket #####

Nulls team of all descendents of a node if their team matches the one provided. Returns a new bracket with the updates applied. Like `updateTeamAbove`, does _not_ update the starting node, only its descendents.

Consider this situation:

```
Team A (ID 1)
        Team A (ID 5)
Team B (ID 2)
                Team D (ID 7)
Team C (ID 3)
        Team D (ID 6)
Team D (ID 4)
```

I've changed my mind and decided Team C will beat Team D in the first round. I update node 6 accordingly:

```
Team A (ID 1)
        Team B (ID 5)
Team B (ID 2)
                Team D (ID 7)
Team C (ID 3)
        Team C (ID 6)
Team D (ID 4)
```

Now we have a problem: Team D is floating out there in node 7, but there's no way Team D could make it there anymore. Calling `nullTeamBelow(bracket, 6, Team D)` would return the following:

```
Team A (ID 1)
        Team B (ID 5)
Team B (ID 2)
                Teamless Node (ID 7)
Team C (ID 3)
        Team C (ID 6)
Team D (ID 4)
```

We nullified the issue at node 7. Basically, this is a cleanup function to run after an update.

## API Endpoints ##

### GET ###

#### /canonical ####

Endpoint to fetch every canonical bracket for a given tournament. Expects no parameters and returns the following payload:

```typescript
        error: null;
        data: Bracket[];
```

#### /bracket/:bracketid ####

Fetches a specific bracket by its ID. Performing a `GET` request on `/bracket/555/` when a bracket with the ID `555` exists in the database will produce the following response payload:

```typescript
        error: null;
        data: {
                bracket: Bracket;
        }
```

Returns the following payload if the bracket does not exist in the database:

```typescript
        error: null;
        data: null;
```

Note that you will still get a 200 status code response if no bracket exists; check for the `null` status of the `data` property to find out whether you received a valid bracket.

### POST ###

#### /bracket ####

Endpoint for creating and updating brackets. Expects the following payload:

```typescript
    bracket: Bracket,
    name?: string;
    email: string;
    conferenceDivision: string;
```

Name is optional. Will throw a 400 error if bracket is malformed or email doesn't match a pretty generous regex.

Checks the database for a row with the given email address and conference+division. If there's a match, merely updates that row to match the new bracket provided. If no match, it inserts a new row with all the information sent in the POST request.

Returns the following payload: 

```typescript
        error: null;
        data: {
                created: boolean;
                id: number;               
        }  
```

`created` is set to `true` if no bracket matching the parameters previously existed and `false` otherwise.